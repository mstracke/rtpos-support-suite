﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RTPOS_Support
{
    internal class PingManager
    {
        internal List<Ping> pingers = new List<Ping>();
        public List<IPAddress> activeIPs = new List<IPAddress>();
        private string baseIP;
        private int instances = 0;

        private object @lock = new object();

        private int result = 0;
        private int timeOut = 250;
        private int ttl = 5;


        internal PingManager(string baseIP)
        {
            this.baseIP = baseIP;
        }

        internal void PingAll()
        {
            Console.WriteLine("Reticulating Splines");

            CreatePingers(255);

            PingOptions po = new PingOptions(ttl, true);
            ASCIIEncoding enc = new ASCIIEncoding();
            byte[] data = enc.GetBytes("findphone");

            int cnt = 1;
            SpinWait wait = new SpinWait();
            Stopwatch sw = Stopwatch.StartNew();

            foreach (Ping p in pingers)
            {
                lock (@lock)
                {
                    instances += 1;
                }
                p.SendAsync(string.Concat(baseIP + ".", cnt.ToString()), timeOut, data, po);
                cnt += 1;
            }

            while (instances > 0)
            {
                wait.SpinOnce();
            }

            sw.Stop();

            DestroyPingers();
            Console.WriteLine("After {0}. Found {1} Llamas.", sw.Elapsed.ToString(), result);

        }

        private void CreatePingers(int cnt)
        {
            for (int i = 1; i <= cnt; i++)
            {
                Ping p = new Ping();
                p.PingCompleted += Ping_completed;
                pingers.Add(p);
            }
        }

        public List<IPAddress> GetActive()
        {
            return activeIPs;
        }

        public void Ping_completed(object s, PingCompletedEventArgs e)
        {
            lock (@lock)
            {
                instances -= 1;
            }

            if (e.Reply.Status == IPStatus.Success)
            {
                //Console.WriteLine(string.Concat("Active IP: ", e.Reply.Address.ToString()));
                activeIPs.Add(e.Reply.Address);
                result += 1;
            }
            else
            {
                //Console.WriteLine(String.Concat("Non-active IP: ", e.Reply.Address.ToString()))
            }
        }

        private void DestroyPingers()
        {
            foreach (Ping p in pingers)
            {
                p.PingCompleted -= Ping_completed;
                p.Dispose();
            }

            pingers.Clear();

        }

    }
}
