﻿
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;


namespace RTPOS_Support
{
    internal class RtAuth
    {
       

        public List<IPAddress> GetGateway()
        {
            List<IPAddress> addresses = new List<IPAddress>();

            NetworkInterface[] interfaces = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface intf in interfaces)
            {
                if (intf == null)
                    continue;

                GatewayIPAddressInformation address = intf.GetIPProperties().GatewayAddresses.FirstOrDefault();
                if (address != null)
                    addresses.Add(address.Address);
            }

            return addresses;
        }


    }
}
