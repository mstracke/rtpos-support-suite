﻿using CefSharp;
using CefSharp.Internals;
using CefSharp.OffScreen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RTPOS_Support
{
    public class CiscoPhoneMonitor
    {
        private readonly string CACHE_PATH = @"C:\RTS\Cache";
        private readonly string FILENAME = @"C:\RTS\src\a.html";

        public Thread monThread;
        private bool bRunSourceEngine = true;
        private string PhoneAddress;
        private CefSettings cefSettings = null;

        private BrowserSettings browserSettings = null;

        private ChromiumWebBrowser browser = null;
        

        public PhoneInfo phoneInfo = new PhoneInfo();

        private RequestContextSettings rqcSettings;
        private bool SafeToRun = true;

        public CiscoPhoneMonitor(string addr)
        {
            this.PhoneAddress = addr;
        }

        public PhoneInfo MyPhoneInfo { get { return this.phoneInfo; } set { this.phoneInfo = value; } }


        public void SetPhoneInfo(PhoneInfo pi)
        {
            MyPhoneInfo = pi;
        }


        public void StartMonitor()
        {
            monThread = new Thread(new ThreadStart(SourceEngine));
            monThread.Start();

            
        }

        public void SetSafe()
        {
            SafeToRun = true;
        }

        public void Shutdown()
        {
            bRunSourceEngine = false;
            browser.Dispose();
        }

        private void SourceEngine()
        {
            while (bRunSourceEngine)
            {
                if (!Cef.IsInitialized)
                {
                    cefSettings = new CefSettings();
                    cefSettings.CachePath = CACHE_PATH;
                    Cef.Initialize(cefSettings);
                }
                if (SafeToRun)
                {
                    SourceEngineLoop();
                }
                //Task.Delay(3000);
            }
        }

        private async void SourceEngineLoop()
        {
            SafeToRun = false;
            Console.WriteLine("Starting Loop");


            browserSettings = new BrowserSettings();
            browserSettings.Javascript = CefState.Disabled;

            rqcSettings = new RequestContextSettings();
            rqcSettings.PersistUserPreferences = true;
            rqcSettings.PersistSessionCookies = true;

            RequestContext rContext = new RequestContext(rqcSettings);
            browser = new ChromiumWebBrowser(PhoneAddress, browserSettings, rContext);


            await LoadPageAsync(browser);

            string source = await browser.GetSourceAsync();
            File.WriteAllText(FILENAME, source);
            await Task.Delay(3000);
            browser.Dispose();
        }

        public static Task LoadPageAsync(IWebBrowser browser, string address = null)
        {
            var tcs = new TaskCompletionSource<bool>();

            EventHandler<LoadingStateChangedEventArgs> handler = null;
            handler = (sender, args) =>
            {
                //Wait for while page to finish loading not just the first frame
                if (!args.IsLoading)
                {
                    browser.LoadingStateChanged -= handler;
                    tcs.TrySetResultAsync(true);
                }
            };

            browser.LoadingStateChanged += handler;

            if (!string.IsNullOrEmpty(address))
            {
                browser.Load(address);
            }
            return tcs.Task;
        }

    }
}
