﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTPOS_Support
{
    public class PhoneInfo
    {

        public string CallState = string.Empty;
        public string CallType = string.Empty;
        public string PeerNumber = string.Empty;

        public PhoneInfo(string _CallState, string _CallType, string _PeerNumber)
        {
            this.CallState = _CallState;
            this.CallType = _CallType;
            this.PeerNumber = _PeerNumber;
        }

        public PhoneInfo()
        {

        }

    }
}
