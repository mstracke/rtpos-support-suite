﻿using CefSharp;
using CefSharp.WinForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTPOS_Support
{
    public class PanelBrowser
    {

        public List<ChromiumWebBrowser> browserList = new List<ChromiumWebBrowser>();
        private string CACHE_PATH = @"C:\RTS\cache";

        public PanelBrowser()
        {
            if (!Cef.IsInitialized)
            {
                CefSettings settings = new CefSettings();
                settings.CachePath = CACHE_PATH;

                Cef.Initialize(settings);
            }
            
        }

        public ChromiumWebBrowser GetPanelBrowser(string name, string addr)
        {
            ChromiumWebBrowser browser = new ChromiumWebBrowser(addr);
            browser.Dock = System.Windows.Forms.DockStyle.Fill;
            browser.Name = name;
            browserList.Add(browser);
            return browser;
        }

        public void ShutDown()
        {
            Cef.Shutdown();
        }

        public ChromiumWebBrowser GetBrowserByName(string name)
        {
            if (browserList.Count > 0)
            {
                foreach (ChromiumWebBrowser br in browserList)
                {
                    if (br.Name == name)
                        return br;
                }
            }
            return null;

        }



    }
}
