﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;
using Timer = System.Timers.Timer;
using System.IO;
using NHtmlUnit;
using NHtmlUnit.Html;
using CefSharp;

namespace RTPOS_Support
{
    public partial class frmMain : Form
    {
        private DataGridView callHistoryGrid;
        private PanelBrowser panelBrowser;
        private Timer SystemTick;

        public string csAddr = "https://cs.rtpos.com/";
        public string slackAddr = "https://rtpos.slack.com";
        public string kbAddr = "http://bgnstudios.com/support/";
       
        private string PhoneHistoryAddress;
        private string DisplayName;

        
        private FileSystemWatcher fileWatch;

        private readonly string FILENAME = @"C:\RTS\src\a.html";

        private string historySource = string.Empty;
        private PhoneHawk phoneHawk;

        public string HistorySource { get {return historySource; } private set {historySource = value; } }

        public string PhoneAddress { get; private set; }
       

        public CiscoPhoneMonitor cpm;

        public frmMain(string phoneIp, string name)
        {

            this.PhoneAddress = phoneIp;
            this.PhoneHistoryAddress = "http://" + phoneIp + "/Call_History.htm";
            this.DisplayName = name;

            InitializeComponent();

            callHistoryGrid = new DataGridView();
            
            callHistoryGrid.BackgroundColor = Color.DimGray;
            callHistoryGrid.ColumnHeadersDefaultCellStyle.BackColor = Color.DimGray;
            callHistoryGrid.ColumnHeadersDefaultCellStyle.ForeColor = Color.Gainsboro;
            callHistoryGrid.ColumnHeadersDefaultCellStyle.Font = new Font(callHistoryGrid.Font, FontStyle.Bold);
            callHistoryGrid.AutoSizeRowsMode = DataGridViewAutoSizeRowsMode.DisplayedCellsExceptHeaders;
            callHistoryGrid.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.Single;
            callHistoryGrid.CellBorderStyle = DataGridViewCellBorderStyle.Single;
            callHistoryGrid.GridColor = Color.Silver;
            callHistoryGrid.RowHeadersVisible = true;
            string[] r = { "Type", "Caller ID", "Number", "Start Time", "End Time", "Duration" };

            callHistoryGrid.ColumnCount = r.Length;

            for(int i = 0; i < r.Length; i++)
            {
                callHistoryGrid.Columns[i].Name = r[i];
            }

            callHistoryGrid.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            callHistoryGrid.MultiSelect = true;
            callHistoryGrid.Dock = DockStyle.Fill;

            

            CallHistoryPanel.Controls.Add(callHistoryGrid);

           

        }

        

        private void Form1_Load(object sender, EventArgs e)
        {
            if (File.Exists(FILENAME))
                File.Delete(FILENAME);


            SystemTick = new Timer(500);
            SystemTick.Elapsed += SystemTick_Elapsed;
            SystemTick.Start();

            panelBrowser = new PanelBrowser();

            SlackPanel.Controls.Add(panelBrowser.GetPanelBrowser("slack", slackAddr));
            KnowledgePanel.Controls.Add(panelBrowser.GetPanelBrowser("kb", kbAddr));
            ClientPortalPanel.Controls.Add(panelBrowser.GetPanelBrowser("cs", csAddr));

           


            lblUser.Text = DisplayName;//TRIM THIS UP
            lblInfo.Text = PhoneHistoryAddress;

            fileWatch = new FileSystemWatcher()
            {
                Path = @"C:\RTS\src\",
                Filter = "*.html"
            };
            fileWatch.Created += new FileSystemEventHandler(OnSourceCreated);
            fileWatch.EnableRaisingEvents = true;

            phoneHawk = new PhoneHawk(PhoneAddress);
            phoneHawk.BeginHawk();
            phoneHawk.SetManagedDNDState(false);
            string addr = string.Format("http://{0}/advanced", PhoneAddress);
            cpm = new CiscoPhoneMonitor(addr);
            cpm.StartMonitor();

        }

        private void OnSourceCreated(object sender, FileSystemEventArgs e)
        {
           
            if (e.Name.Equals("a.html"))
                GetPhoneInfo(e.FullPath);

        }

        

        private void GetPhoneInfo(string fullPath)
        {
            WebClient wc = new WebClient(BrowserVersion.FIREFOX_17);
            wc.Options.JavaScriptEnabled = false;
            wc.Options.CssEnabled = false;
            wc.Options.ThrowExceptionOnScriptError = false;

            HtmlPage infoPage = wc.GetHtmlPage(new Uri(fullPath).AbsoluteUri.ToString());

            HtmlDivision infoDiv = infoPage.GetElementById<HtmlDivision>("Info");

            HtmlTableRow line1Row = null;
            HtmlTableRow callStateRow = null;
            HtmlTableRow callTypeRow = null;
            HtmlTableRow peerPhoneRow = null;

            int count = 0;
            int index = 0;
            foreach (HtmlTable table in infoDiv.GetHtmlElementsByTagName("table"))
            {
                count = 0;
                foreach (HtmlTableBody body in table.Bodies)
                {
                    foreach (HtmlTableRow row in body.Rows)
                    {
                        if (row.AsText().Trim().Equals("Line 1 Call 1 Status"))
                        {
                            line1Row = row;
                            callStateRow = body.Rows[count + 1];
                            callTypeRow = body.Rows[count + 3];
                            peerPhoneRow = body.Rows[count + 4];

                        }
                        count++;
                    }
                }

            }

            //STATUS
            count = 0;
            foreach (HtmlTableCell cell in callStateRow.Cells)
            {
                if (cell.AsText().Trim().Equals("Call State"))
                {
                    index = count;
                    count++;
                }
            }
            HtmlTableCell callstateCell = callStateRow.GetCell(index + 1);

            string callstate = callstateCell.AsText();



            //TYPE
            count = 0;
            foreach (HtmlTableCell cell in callTypeRow.Cells)
            {
                if (cell.AsText().Trim().Equals("Type"))
                {
                    index = count;
                    count++;
                }
            }
            HtmlTableCell calltypecell = callTypeRow.GetCell(index + 1);
            string calltype = calltypecell.AsText();


            //PEER
            count = 0;
            foreach (HtmlTableCell cell in peerPhoneRow.Cells)
            {
                if (cell.AsText().Trim().Equals("Peer Phone"))
                {
                    index = count;
                    count++;
                }
            }
            HtmlTableCell peercell = peerPhoneRow.GetCell(index + 1);
            string peer = peercell.AsText();



            cpm.SetPhoneInfo(new PhoneInfo(callstate, calltype, peer));

            wc.CloseAllWindows();
            File.Delete(FILENAME);
            cpm.SetSafe();
        }

       
        
        

        

        private void SystemTick_Elapsed(object sender, ElapsedEventArgs e)
        {
            UpdateTime(DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString());

            UpdateCallHistory();

            UpdateInfo();

        }

        private void UpdateInfo()
        {
            string text = string.Format("Phone Status: {0}\nCurrent Call: {1} - {2}\nDesk Status: {3}", cpm.MyPhoneInfo.CallState, cpm.MyPhoneInfo.CallType, cpm.MyPhoneInfo.PeerNumber, phoneHawk.GetPhoneDNDState().ToString());

            if (lblInfo.InvokeRequired)
            {
                lblInfo.BeginInvoke((MethodInvoker)delegate ()
                {
                    lblInfo.Text = text;
                });
            } else
            {
                lblInfo.Text = text;
            }
        }

        private void UpdateCallHistory()
        {
            bool bUpdate = false;
            List<Caller> history = phoneHawk.GetCallHistory();
            if(callHistoryGrid.Rows.Count > 1)
            {
                int count = 0;
                foreach (DataGridViewRow item in callHistoryGrid.Rows)
                {
                    if(count < history.Count)
                    {
                        if (!item.Cells[2].Value.Equals(history[count].PhoneNumber))
                        {
                            bUpdate = true;
                        }
                    }
                   
                    count++;
                }
            }
            else
            {
                bUpdate = true;
            }

            if (bUpdate)
            {

                if (callHistoryGrid.InvokeRequired)
                {
                    callHistoryGrid.BeginInvoke((MethodInvoker)delegate ()
                    {
                        callHistoryGrid.Rows.Clear();
                    });
                } else
                {
                    callHistoryGrid.Rows.Clear();
                }


                foreach (Caller c in history)
                {
                    //string[] r = { "Type", "Caller ID", "Number", "Start Time", "End Time", "Duration" };
                    string[] row = { c.CallType, c.CallerId, c.PhoneNumber, c.StartTime, c.EndTime, c.Duration };
                    if (callHistoryGrid.InvokeRequired)
                    {
                        callHistoryGrid.BeginInvoke((MethodInvoker)delegate ()
                        {
                            callHistoryGrid.Rows.Add(row);
                            callHistoryGrid.Refresh();
                        });
                    }
                    else
                    {
                        callHistoryGrid.Rows.Add(row);
                        callHistoryGrid.Refresh();
                    }
                }
            }

            
        }

        private void UpdateTime(string v)
        {
            if (tsTime.InvokeRequired)
            {
                tsTime.BeginInvoke((MethodInvoker)delegate () 
                {
                    lblTimeLogo.Text = v;
                    tsTime.Refresh();
                });
            } else
            {
                lblTimeLogo.Text = v;
                tsTime.Refresh();
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (Cef.IsInitialized)
            {
                Cef.Shutdown();
            }
            phoneHawk.ShutDown();
            cpm.Shutdown();
            panelBrowser.ShutDown();
            this.Dispose();
            Application.Exit();
            Environment.Exit(0);
        }


        #region Panel Browser Buttons
        private void toolStripButton11_Click_1(object sender, EventArgs e)
        {
            panelBrowser.GetBrowserByName("slack").Load(slackAddr);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            if (panelBrowser.GetBrowserByName("slack").CanGoBack)
                panelBrowser.GetBrowserByName("slack").GetBrowser().GoBack();
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            panelBrowser.GetBrowserByName("slack").GetBrowser().Reload();
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (panelBrowser.GetBrowserByName("slack").CanGoForward)
                panelBrowser.GetBrowserByName("slack").GetBrowser().GoForward();
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            panelBrowser.GetBrowserByName("cs").Load(csAddr);
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            if (panelBrowser.GetBrowserByName("cs").CanGoBack)
                panelBrowser.GetBrowserByName("cs").GetBrowser().GoBack();
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            panelBrowser.GetBrowserByName("cs").GetBrowser().Reload();
        }

        private void toolStripButton8_Click(object sender, EventArgs e)
        {
            if (panelBrowser.GetBrowserByName("cs").CanGoForward)
                panelBrowser.GetBrowserByName("cs").GetBrowser().GoForward();
        }

        private void toolStripButton9_Click(object sender, EventArgs e)
        {
            panelBrowser.GetBrowserByName("kb").Load(kbAddr);
        }

        private void toolStripButton10_Click(object sender, EventArgs e)
        {
            if (panelBrowser.GetBrowserByName("kb").CanGoBack)
                panelBrowser.GetBrowserByName("kb").GetBrowser().GoBack();
        }

        private void toolStripButton12_Click(object sender, EventArgs e)
        {
            panelBrowser.GetBrowserByName("kb").GetBrowser().Reload();
        }

        private void toolStripButton13_Click(object sender, EventArgs e)
        {
            if (panelBrowser.GetBrowserByName("kb").CanGoForward)
                panelBrowser.GetBrowserByName("kb").GetBrowser().GoForward();
        }
        #endregion

        #region Main Buttons
        private void btnTimeclock_Click(object sender, EventArgs e)
        {
            MessageBox.Show(this, "Not Yet Implemented", "Future Update", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        #endregion

        private void minToolStripMenuItem_Click(object sender, EventArgs e)
        {
            RequestBreak(5);
        }

        private void minToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            RequestBreak(10);
        }

        private void minToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            RequestBreak(15);
        }

        private void minToolStripMenuItem3_Click(object sender, EventArgs e)
        {
            RequestBreak(20);
        }

        private void RequestBreak(int v)
        {
            
        }
    }
}
