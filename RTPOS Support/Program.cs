﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace RTPOS_Support
{
    static class Program
    {
        private static RtAuth rtAuth;


        public static string PhoneAddress { get; private set; }
        public static string DisplayName { get; private set; }


        [STAThread]
        static void Main()
        {
            PhoneAddress = string.Empty;
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.WriteLine("RT Support: Authenticating..");
           
            DoAuth();
            Console.Clear();
            
            if (PhoneAddress != string.Empty)
            {
                Console.WriteLine(string.Format("Auth Success, Welcome {0}", DisplayName));
                
                ShowWindow(GetConsoleWindow(), SW_HIDE);
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmMain(PhoneAddress, DisplayName));
            }
            else
            {
                MessageBox.Show(null, "No Phone Found, Exiting", "Invalid Auth", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(0);
            }



        }

        internal static async void DoAuth()
        {
            await RTPhoneAuth();
        }

        private static Task RTPhoneAuth()
        {
            var tcs = new TaskCompletionSource<bool>();

            rtAuth = new RtAuth();
            List<IPAddress> addressList = rtAuth.GetGateway();
            string addrStr = string.Empty;
            foreach (IPAddress addr in addressList)
            {
                if (addr.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    addrStr = addr.ToString();

            }

            int lInd = addrStr.LastIndexOf('.');
            string leadAddr = addrStr.Substring(0, lInd);

            PingManager pm = new PingManager(leadAddr);
            pm.PingAll();
            List<IPAddress> usedAddresses = pm.GetActive();

            foreach (IPAddress addr in usedAddresses)
            {
                if (ValidateIfPhone(addr.ToString()))
                    break;
            }

            return tcs.Task;
        }

        private static bool ValidateIfPhone(string addr)
        {
            PhoneValidater pv = new PhoneValidater(addr);
            if (pv.Validate())
            {
                PhoneAddress = pv.PhoneAddress;
                DisplayName = pv.DisplayName;
                return true;
            }
            return false;
        }

        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;
    }
}
