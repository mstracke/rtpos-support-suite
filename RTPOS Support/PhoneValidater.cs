﻿using NHtmlUnit;
using NHtmlUnit.Html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RTPOS_Support
{
    internal class PhoneValidater
    {
        private string addr;

        private string displayName = string.Empty;
        public string DisplayName
        {
            get
            {
                return this.displayName;
            }
            private set
            {
                this.displayName = ParseDisplayName(value);
            }
        }

        private string ParseDisplayName(string value)
        {
            string nVal = value.Trim();
            nVal = nVal.Replace("Display Name", "");
            return nVal;
            //return nVal.Substring(nVal.IndexOf('"'), nVal.LastIndexOf('"'));
        }

        public string PhoneAddress { get; private set; }

        public PhoneValidater(string addr)
        {
            this.addr = addr;
        }



        internal bool Validate()
        {
            WebClient wc = new WebClient(BrowserVersion.FIREFOX_17);
            wc.Options.CssEnabled = false;
            wc.Options.JavaScriptEnabled = false;

            try
            {
                HtmlPage p = wc.GetHtmlPage("http://" + addr + "/");

                if (p.AsXml().Contains("Cisco"))
                {
                    if (GetPhoneUser(p, wc))
                    {
                        PhoneAddress = addr;
                        wc.CloseAllWindows();
                        return true;
                    }

                }

            }
            catch (Exception)
            {
                wc.CloseAllWindows();
                return false;
            }


            wc.CloseAllWindows();
            return false;
        }

        private bool GetPhoneUser(HtmlPage p, WebClient wc)
        {

            HtmlDivision ext1Div = p.GetElementById<HtmlDivision>("Ext 1");
            foreach (var tbody in ext1Div.GetElementsByTagName("tbody"))
            {
                //Console.WriteLine(((HtmlTableBody)tbody).AsXml());

                foreach (var t in ((HtmlElement)tbody).GetElementsByTagName("tr"))
                    if (((HtmlElement)t).AsXml().Contains("Display"))
                    {
                        DisplayName = ((HtmlElement)t).AsText();
                        return true;
                    }




            }
            return false;
        }
    }
}
