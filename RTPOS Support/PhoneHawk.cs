﻿using NHtmlUnit;
using NHtmlUnit.Html;
using NHtmlUnit.W3C.Dom;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RTPOS_Support
{
    public class PhoneHawk
    {
        private string callHistoryAddr = string.Empty;
       

        public Thread HawkThread { get; private set; }
        private volatile bool firstRun = true;

        private string phoneAddr = string.Empty;

        public volatile bool bRunPhoneWatch = true;

        public bool managedDND = false;

        public List<Caller> CallHistory;

        public string PhoneState;
        public bool PhoneDNDState;

        private WebClient wc;

        private Stopwatch sw = new Stopwatch();
        private long miliMin = 3000;

        public PhoneHawk(string addr)
        {
            phoneAddr = addr;
            HawkThread = new Thread(new ThreadStart(PhoneWatch));
        }

        public void BeginHawk()
        {
            CallHistory = new List<Caller>();
            HawkThread.Start();
        }

        public bool GetManagagedDNDState()
        {
            return managedDND;
        }

        public void SetManagedDNDState(bool val)
        {
            this.managedDND = val;
        }

        private void StartWebClient()
        {
            wc = new WebClient(BrowserVersion.FIREFOX_17);
            wc.Options.CssEnabled = false;
            //wc.Options.JavaScriptEnabled = true;
            wc.Options.ThrowExceptionOnScriptError = false;
            wc.Options.ThrowExceptionOnFailingStatusCode = false;
        }

        public bool GetPhoneState()
        {
            return PhoneState == "Idle" ? false : true;
        }

        public bool GetPhoneDNDState()
        {
            return PhoneDNDState;
        }

        public void PhoneWatch()
        {
            StartWebClient();

            while (bRunPhoneWatch && phoneAddr != string.Empty)
            {
                if (!sw.IsRunning)
                    sw.Start();


                if (firstRun || sw.ElapsedMilliseconds >= miliMin)
                {

                    HtmlPage mainPage = null;
                    string phoneStatus = string.Empty;
                    bool dndStatus = false;
                    List<Caller> callHistory = new List<Caller>();
                    string addr = string.Format("http://{0}/advanced", phoneAddr);
                    mainPage = wc.GetHtmlPage(addr);
                    if (Hawk_GetPhoneDND(mainPage, out dndStatus)) 
                    {
                        callHistory = Hawk_GetPhoneCallHistory(wc);
                    }
                    
                    
                    PhoneDNDState = dndStatus;
                    CallHistory = callHistory;
                    firstRun = false;
                    sw.Restart();
                    
                    wc.CloseAllWindows();
                }
            }
        }


        

        public bool Hawk_GetPhoneDND(HtmlPage mPage, out bool DND)
        {
            Console.WriteLine("Getting Phone DND State");
            HtmlElement uDiv = null;
            HtmlDivision userDiv = (HtmlDivision)mPage.GetElementById("User");
            IList<INode> nodes = userDiv.GetElementsByTagName("select");
            foreach (HtmlElement ele in nodes)
            {
                if (ele.GetAttribute("name") == "42030")
                    uDiv = ele;
            }
            HtmlSelect select = (HtmlSelect)uDiv;

            bool dndEnabled = ParseDnd(select.SelectedOptions.First());
            //Console.WriteLine("DND: " + dndEnabled.ToString());
            if (dndEnabled != managedDND)
            {
                select.SetSelectedAttribute(GetOption(select, managedDND), true);
                IList<object> submits = mPage.GetByXPath(@"/html/body/center/table/tbody/tr[6]/td/table/tbody/tr/td[2]/input");
                foreach (object obj in submits)
                {
                    if (((HtmlSubmitInput)obj).GetAttribute("value").Equals("Submit All Changes"))
                        ((HtmlSubmitInput)obj).Click();
                }
            }
            DND = dndEnabled;
            return true;
        }

        public List<Caller> Hawk_GetPhoneCallHistory(WebClient web)
        {
            Console.WriteLine("Getting Phone Call History");
            web.Options.JavaScriptEnabled = true;
            List<Caller> cHistory = new List<Caller>();
            callHistoryAddr = string.Format("http://{0}/Call_History.htm", phoneAddr);
            HtmlPage callHistory = web.GetHtmlPage(callHistoryAddr);

            IList<FrameWindow> pageFrames = callHistory.Frames;
            HtmlPage hPage = null;
            foreach (FrameWindow fwind in pageFrames)
            {
                if (fwind.EnclosedPage.Url.ToString().Contains("Call_History_List"))
                    hPage = (HtmlPage)fwind.EnclosedPage;
            }

            if (hPage != null)
            {
                HtmlTable historyTable = hPage.GetElementById<HtmlTable>("tb_call_history_list");
                IList<HtmlTableBody> tableBodies = historyTable.Bodies;

                HtmlTableBody tableBody = tableBodies.First();
                IList<HtmlTableRow> rows = tableBody.Rows;

                Caller caller = null;

                foreach (HtmlTableRow Row in rows)
                {
                    caller = new Caller();

                    if (Row.AsText().Contains("Phone Number"))
                        continue;

                    IList<HtmlTableCell> Cells = Row.Cells;
                    int index = 0;
                    foreach (HtmlTableCell Cell in Cells)
                    {
                        if (index == 2)
                            caller.PhoneNumber = Cell.AsText();

                        if (index == 3)
                            caller.CallerId = Cell.AsText();

                        if (index == 4)
                            caller.StartTime = Cell.AsText();

                        if (index == 5)
                            caller.EndTime = Cell.AsText();

                        if (index == 6)
                            caller.Duration = Cell.AsText();

                        if(index == 7)
                        {
                            if (Cell.AsXml().Contains("image/UI_outgoing.gif"))
                                caller.CallType = "Outbound";
                            if (Cell.AsXml().Contains("image/UI_answered.gif"))
                                caller.CallType = "Inbound";
                            if (Cell.AsXml().Contains("image/UI_missed.gif"))
                                caller.CallType = "Missed";
                        }
                            

                        index++;
                    }
                    cHistory.Add(caller);
                }


            }

            return cHistory;


        }

        internal void ShutDown()
        {
            bRunPhoneWatch = false;
            //wc.CloseAllWindows();
        }

        public List<Caller> GetCallHistory()
        {
            return this.CallHistory;
        }

        private HtmlOption GetOption(HtmlSelect sel, bool status)
        {
            foreach (HtmlOption option in sel.Options)
            {
                if (ParseDnd(option) == status)
                    return option;
            }
            return null;
        }

        private bool ParseDnd(HtmlOption opt)
        {
            if (opt.AsXml().Contains("value=\"1\""))
                return true;
            else
                return false;
        }


    }
}
